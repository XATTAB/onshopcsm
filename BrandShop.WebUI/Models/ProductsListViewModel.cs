﻿using System.Collections.Generic;
using BrandShop.Domain.Entities;
using BrandShop.Domain.EntitiesDb;

namespace BrandShop.WebUI.Models
{
    public class ProductsListViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentCategory { get; set; }
        public OrderFilter Filter { get; set; }
    }
}