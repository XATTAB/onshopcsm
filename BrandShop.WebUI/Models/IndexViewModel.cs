﻿using System.Collections.Generic;
using BrandShop.Domain.Entities;
using BrandShop.Domain.EntitiesDb;

namespace BrandShop.WebUI.Models
{
    public class IndexViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<Product> ProductsForSlider { get; set; }
    }
}