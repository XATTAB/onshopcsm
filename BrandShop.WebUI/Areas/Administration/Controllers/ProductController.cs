﻿using System.Web;
using System.Web.Mvc;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;

namespace BrandShop.WebUI.Areas.Administration.Controllers
{
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            return View();
        }

        public PartialViewResult ShowAll()
        {
            return PartialView(_unitOfWork.ProductRepository.GetAll);
        }

        public ViewResult Edit(int productId)
        {
            return View(_unitOfWork.ProductRepository.Get(productId));
        }

        private void AddImageToProduct(Product product, HttpPostedFileBase[] images)
        {
            foreach (var httpPostedFileBase in images)
            {
                if (httpPostedFileBase == null) continue;

                var imagePr = new ProductImage
                {
                    Product = product,
                    ImageMimeType = httpPostedFileBase.ContentType,
                    ImageData = new byte[httpPostedFileBase.ContentLength],
                    PreviewImageMimeType = httpPostedFileBase.ContentType
                };

                httpPostedFileBase.InputStream.Read(imagePr.ImageData, 0, httpPostedFileBase.ContentLength);

                _unitOfWork.ProductImageRepository.Create(imagePr);
            }
        }

        [HttpPost]
        public ActionResult Edit(Product product, HttpPostedFileBase[] images = null)
        {
            if (!ModelState.IsValid) return View(product);

            AddImageToProduct(product, images);

            _unitOfWork.ProductRepository.Update(product);
            _unitOfWork.Save();

            TempData["message"] = $"Изменения в товаре \u2033{product.Name}\u2033 были сохранены";

            return RedirectToAction("Index");
        }

        public ViewResult Create()
        {
            return View(new Product());
        }

        [HttpPost]
        public ActionResult Create(Product product, HttpPostedFileBase[] images = null)
        {
            if (!ModelState.IsValid) return View(product);

            var tempProd = _unitOfWork.ProductRepository.Get(product.ProductId);
            if (tempProd != null) product.ProductImages = tempProd.ProductImages;

            AddImageToProduct(product, images);

            _unitOfWork.ProductRepository.Create(product);
            _unitOfWork.Save();

            TempData["message"] = $"Изменения в товаре \u2033{product.Name}\u2033 были сохранены";

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int productId)
        {
            var product = _unitOfWork.ProductRepository.Get(productId);

            if (product != null)
            {
                var deleteProduct = _unitOfWork.ProductRepository.Delete(productId);

                TempData["message"] = $"Продукт \u2033{deleteProduct.Name}\u2033 был удален";
                _unitOfWork.Save();
            }

            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult DeleteImage(int imageId)
        {
            _unitOfWork.ProductImageRepository.Delete(imageId);
            _unitOfWork.Save();

            var referrer = HttpContext.Request.UrlReferrer?.ToString();
            return Redirect(referrer ?? Url.Action("Index", "Product"));
        }
    }
}