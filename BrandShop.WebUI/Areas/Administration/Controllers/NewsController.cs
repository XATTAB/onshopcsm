﻿using System.Web.Mvc;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;

namespace BrandShop.WebUI.Areas.Administration.Controllers
{
    [Authorize]
    public class NewsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public NewsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult ShowAll()
        {
            return PartialView(_unitOfWork.NewsRepository.GetAll);
        }

        public ViewResult Create()
        {
            return View(new News());
        }

        [HttpPost]
        public ActionResult Create(News news)
        {
            if (!ModelState.IsValid) return View(news);

            _unitOfWork.NewsRepository.Create(news);

            _unitOfWork.Save();

            TempData["message"] = $"Новость \u2033{news.Header}\u2033 добавлена!";

            return RedirectToAction("Index");
        }

        public ViewResult Edit(int newsId)
        {
            var news = _unitOfWork.NewsRepository.Get(newsId);
            return View(news);
        }

        [HttpPost]
        public ActionResult Edit(News news)
        {
            if (!ModelState.IsValid) return View(news);

            _unitOfWork.NewsRepository.Update(news);

            _unitOfWork.Save();

            TempData["message"] = $"Новость \u2033{news.Header}\u2033 сохранена!";

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int newsId)
        {
            var deletenews = _unitOfWork.NewsRepository.Delete(newsId);
            if (deletenews != null)
            {
                TempData["message"] = $"Новость \u2033{deletenews.Header}\u2033 была удалена";
                _unitOfWork.Save();
            }
            return RedirectToAction("Index");
        }
    }
}