﻿using System.Linq;
using System.Web.Mvc;
using BrandShop.Domain.Interfaces;
using BrandShop.WebUI.Models;

namespace BrandShop.WebUI.Controllers
{
    public class InformationController : Controller
    {
        private const int AmountProductsOnHomePage = 8;
        private readonly IUnitOfWork _unitOfWork;

        public InformationController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            var products = _unitOfWork.ProductRepository.GetAll
                .Reverse()
                .Take(AmountProductsOnHomePage)
                .ToList();

            var productsForSlider = _unitOfWork.ProductRepository.GetAll.
                Where(x => x.ForMain)
                .Reverse()
                .ToList();

            var model = new IndexViewModel
            {
                Products = products,
                ProductsForSlider = productsForSlider
            };

            return View(model);
        }

        public ViewResult Contacts()
        {
            return View();
        }

        public ViewResult Delivery()
        {
            return View();
        }

        public ViewResult News()
        {
            var model = _unitOfWork.NewsRepository.GetAll.Reverse().ToList();
            return View(model);
        }
    }
}