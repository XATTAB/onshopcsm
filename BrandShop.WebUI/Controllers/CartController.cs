﻿using System;
using System.Linq;
using System.Web.Mvc;
using BrandShop.Domain.Entities;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;
using BrandShop.WebUI.Models;

namespace BrandShop.WebUI.Controllers
{
    public class CartController : Controller
    {
        private readonly IOrderProcessor _orderProcessor;

        private readonly IUnitOfWork _unitOfWork;

        public CartController(IOrderProcessor orderProcessor, IUnitOfWork unitOfWork)
        {
            _orderProcessor = orderProcessor;
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index(Cart cart, string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = cart,
                ReturnUrl = returnUrl
            });
        }

        public RedirectToRouteResult AddToCart(Cart cart, int productId, string returnUrl, string size)
        {
            if (size == "") return RedirectToAction("View", "Product", new { productId, sizeError = true });
            
            var prod = _unitOfWork.ProductRepository.GetAll.FirstOrDefault(product => product.ProductId == productId);

            if (prod != null)
            {
                cart.AddItem(prod, 1, size);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToRouteResult RemoveFromCart(Cart cart, int productId, string returnUrl)
        {
            var prod = _unitOfWork.ProductRepository.Get(productId);

            if (prod != null)
            {
                cart.RemoveLine(prod);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToRouteResult Remove(Cart cart, int productId, string returnUrl)
        {
            var prod = _unitOfWork.ProductRepository.Get(productId);

            if (prod != null)
            {
                cart.RemoveLine(prod);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public PartialViewResult Summary(Cart cart)
        {
            return PartialView(cart);
        }

        public ViewResult Checkout()
        {
            return View(new ShippingDetails());
        }

        [HttpPost]
        public ViewResult Checkout(Cart cart, ShippingDetails shippingDetails)
        {
            if (!cart.Lines.Any())
            {
                ModelState.AddModelError("", "Извините, ваша корзина пуста!");
            }

            if (shippingDetails.TypeDelivery == null)
            {
                ModelState.AddModelError("", "Укажите способ доставки товара!");
            }

            if (ModelState.IsValid)
            {
                var order = new Order
                {
                    Address = shippingDetails.Address,
                    FullName = shippingDetails.ToString(),
                    Email = shippingDetails.Email,
                    PhoneNumber = shippingDetails.PhoneNumber,
                    OrderStatus = OrderStatus.New,
                    CreateDateTime = DateTime.Now,
                    TypeDelivery = shippingDetails.TypeDelivery.Value,
                    OrderItems = cart.Lines.ToList()
                };

                _unitOfWork.OrderRepository.Create(order);

                _orderProcessor.ProcessOrder(cart, shippingDetails);
                cart.Clear();
                return View("Completed");
            }

            return View(shippingDetails);
        }
    }
}