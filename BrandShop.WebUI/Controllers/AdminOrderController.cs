﻿using System.Linq;
using System.Web.Mvc;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;

namespace BrandShop.WebUI.Controllers
{
    [Authorize]
    public class AdminOrderController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public AdminOrderController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult Index(OrderStatus status = OrderStatus.New)
        {
            return View(_unitOfWork.OrderRepository.GetWhere(x => x.OrderStatus == status).Reverse());
        }

        public string GetTotallSum(int orderId)
        {
            var order = _unitOfWork.OrderRepository.Get(orderId);

            var sum = order.OrderItems.ToList().Sum(x => x.Quantity*x.Product.Price);

            return sum.ToString("# руб.");
        }

        public string GetActive(string query, string thisStatus)
        {
            if (query.Length == 0 && thisStatus == "New") return "active";
            if (query.Length == 0) return string.Empty;

            return query.Remove(0, 8) == thisStatus ? "active" : string.Empty;
        }

        public ActionResult DeleteOrder(int orderId)
        {
            var deleteOrder = _unitOfWork.OrderRepository.Delete(orderId);
            if (deleteOrder != null)
            {
                TempData["message"] = $"Заказ \"{deleteOrder.OrderId}\" был удален";
                _unitOfWork.Save();
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ViewResult View(int orderId)
        {
            var order = _unitOfWork.OrderRepository.Get(orderId);
            return View("View", order);
        }

        [HttpPost]
        public ActionResult View(Order order)
        {
            // TODO Проверить есть ли тут рекурсия
            if (!ModelState.IsValid) return View(order);

            if(order.OrderId == 0)
                _unitOfWork.OrderRepository.Create(order);
            else
                _unitOfWork.OrderRepository.Update(order);

            _unitOfWork.Save();

            TempData["message"] = $"Изменения в заказе \"{order.OrderId}\" были сохранены";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddOrderLine(int orderId, int productId = 0, string size = "", int quantity = 0)
        {
            if(productId == 0 || quantity == 0 || size == "") return RedirectToAction("View", new { orderId });

            var product = _unitOfWork.ProductRepository.Get(productId);

            if(product == null) return RedirectToAction("View", new { orderId });

            var orderLine = new OrderItem
            {
                Product = product,
                Quantity = quantity,
                SizeProduct = size
            };

            var order = _unitOfWork.OrderRepository.Get(orderId);
            order.OrderItems.Add(orderLine);

            _unitOfWork.OrderRepository.Update(order);
            _unitOfWork.Save();

            return RedirectToAction("View", new {orderId});
        }

        public ActionResult DeleteOrderLine(int orderId, int orderItemId)
        {
            _unitOfWork.OrderItemRepository.Delete(orderItemId);
            _unitOfWork.Save();

            return RedirectToAction("View", new { orderId });
        }
    }
}