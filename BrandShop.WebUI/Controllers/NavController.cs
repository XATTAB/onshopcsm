﻿using System.Linq;
using System.Web.Mvc;
using BrandShop.Domain.Interfaces;

namespace BrandShop.WebUI.Controllers
{
    public class NavController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public NavController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public PartialViewResult Menu(string category = null)
        {
            ViewBag.SelectedCategory = category;

            var categories = _unitOfWork.ProductRepository.GetAll
                .Select(product => product.Category)
                .Distinct()
                .OrderBy(x => x);

            return PartialView(categories);
        }

        public string GetCurrent(string[] segments, string name)
       {
           if (segments.Length< 2)
               return null;

           return name.ToLower() != segments[1].ToLower() ? null : "class=current";
       }
}
}