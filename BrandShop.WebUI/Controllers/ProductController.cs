﻿using System.Linq;
using System.Web.Mvc;
using BrandShop.Domain.Entities;
using BrandShop.Domain.Interfaces;
using BrandShop.WebUI.HtmlHelpers;
using BrandShop.WebUI.Models;

namespace BrandShop.WebUI.Controllers
{
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public int PageSize = 12;

        public ProductController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        private void SortProducts(ProductsListViewModel model, OrderFilter filter)
        {
            var prFilter = model.Products.ToList();

            switch (filter)
            {
                case OrderFilter.FirstNew:
                    {
                        prFilter.Reverse();
                        break;
                    }
                case OrderFilter.FirstCheap:
                    {
                        prFilter.Sort((x, y) => x.Price.CompareTo(y.Price));
                        break;
                    }
                case OrderFilter.FirstPricey:
                    {
                        prFilter.Sort((x, y) => x.Price.CompareTo(y.Price));
                        prFilter.Reverse();
                        break;
                    }
            }

            model.Products = prFilter;
        }

        public ViewResult List(string category, OrderFilter filter = OrderFilter.FirstNew, int page = 1)
        {
            var model = new ProductsListViewModel
            {
                Products = _unitOfWork.ProductRepository.GetAll
                    .Where(p => category == null || p.Category == category)
                    .OrderBy(product => product.ProductId),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null ? _unitOfWork.ProductRepository.GetAll.Count()
                        : _unitOfWork.ProductRepository.GetAll.Count(product => product.Category == category)
                },
                CurrentCategory = category
            };

            SortProducts(model, filter);

            model.Products = model.Products.Skip((page - 1) * PageSize).Take(PageSize);
            model.Filter = filter;

            return View(model);
        }

        [HttpPost]
        public ActionResult List(string category, int filter)
        {
            return RedirectToAction("List", new { category, filter });
        }

        public ViewResult View(int productId, bool sizeError = false)
        {
            if (sizeError) ModelState.AddModelError("Size", "Пожалуйста выберите нужный размер!");

            var model = _unitOfWork.ProductRepository.GetAll.FirstOrDefault(x => x.ProductId == productId);

            ViewBag.SizeArray = model?.Sizes?.Replace(" ", "").Split(',');

            return View(model);
        }

        public FileContentResult GetImage(int imageId, bool isPreview = false)
        {
            var iamge = _unitOfWork.ProductImageRepository.Get(imageId);

            if (iamge == null) return null;

            return isPreview ? 
                    File(iamge.PreviewImageData, iamge.PreviewImageMimeType) : 
                    File(ImageMarker.StickMark(iamge.ImageData), iamge.ImageMimeType);
        }
    }
}