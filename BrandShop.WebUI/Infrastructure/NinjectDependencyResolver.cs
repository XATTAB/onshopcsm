﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using BrandShop.Domain.Concrete;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;
using BrandShop.Domain.Settings;
using BrandShop.WebUI.Infrastructure.Abstract;
using BrandShop.WebUI.Infrastructure.Concrete;
using Ninject;

namespace BrandShop.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            _kernel.Bind<IRepository<Product>>().To<EfProductRepository>();
            _kernel.Bind<IAuthProvider>().To<FormAuthProvider>();
            _kernel.Bind<IRepository<News>>().To<EfNewsRepository>();
            _kernel.Bind<IRepository<Order>>().To<EfOrderRepository>();
            _kernel.Bind<IUnitOfWork>().To<EfUnitOfWork>();

            var emailSettings = new EmailSettings
            {
                WriteAsFile = bool.Parse(ConfigurationManager.AppSettings["Email.WriteAsFile"] ?? "false")
            };

            _kernel.Bind<IOrderProcessor>().To<EmailOrderProcessor>()
                .WithConstructorArgument("settings", emailSettings);
        }
    }
}