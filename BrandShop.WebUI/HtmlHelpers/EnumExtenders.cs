﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BrandShop.WebUI.HtmlHelpers
{
    public static class EnumExtenders
    {
        public static string ToStringDesctiption(this Enum enumerate)
        {
            var type = enumerate.GetType();
            var fieldInfo = type.GetField(enumerate.ToString());
            var attributes =
                (DescriptionAttribute[]) fieldInfo.GetCustomAttributes(typeof (DescriptionAttribute), false);
            return attributes.Length != 0 ? attributes[0].Description : enumerate.ToString();
        }

        public static List<string> Descriptions(this Enum enumerate)
        {
            var values = Enum.GetValues(enumerate.GetType());
            var descriptions = new List<String>(values.Length);
            descriptions.AddRange(from object enumVal in values select ((Enum) enumVal).ToStringDesctiption());

            return descriptions;
        }
    }
}