﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace BrandShop.WebUI.HtmlHelpers
{
    public static class ImageResizer
    {
        public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }
        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        public static byte[] ResizeImage(byte[] imageBytes)
        {
            var srcImag = ByteArrayToImage(imageBytes);
            var srcWidth = srcImag.Width/270;
            var srcHeight = srcImag.Height/270;

            var newWidth = srcImag.Width/srcWidth;
            var newHeight = srcImag.Height/srcHeight;

            Image result = new Bitmap(newWidth, newHeight);
            using (Graphics g = Graphics.FromImage(result))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(srcImag, 0, 0, newWidth, newHeight);
                g.Dispose();
            }
            return ImageToByteArray(result);
        }
    }
}