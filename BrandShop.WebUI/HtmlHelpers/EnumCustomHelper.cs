﻿using System.Collections.Generic;
using System.Web.Mvc;
using BrandShop.Domain.Entities;
using BrandShop.Domain.EntitiesDb;

namespace BrandShop.WebUI.HtmlHelpers
{
    public static class EnumCustomHelper
    {
        public static List<SelectListItem> GetSelectList(OrderStatus selected)
        {
            var result = new List<SelectListItem>();

            var listEnum = new List<OrderStatus>
            {
                OrderStatus.New,
                OrderStatus.Processing,
                OrderStatus.Delivered,
                OrderStatus.NotTake
            };

            foreach (var orderStatuse in listEnum)
            {
                var item = new SelectListItem
                {
                    Text = orderStatuse.ToStringDesctiption(),
                    Value = ((int) orderStatuse).ToString()
                };

                if (orderStatuse == selected)
                    item.Selected = true;

                result.Add(item);
            }
            
            return result;
        }
    }
}