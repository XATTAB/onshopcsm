﻿using System.Drawing;
using System.Web.Helpers;

namespace BrandShop.WebUI.HtmlHelpers
{
    public static class ImageMarker
    {
        public static byte[] StickMark(byte[] imageBytes)
        {
            var watermarkPhoto = new WebImage(@"~\Content\images\watermark\mark.png");
            
            var photo = new WebImage(imageBytes);

            photo.AddImageWatermark(watermarkPhoto, width: 98, height: 100, horizontalAlign: "Right", verticalAlign: "Bottom", opacity: 100, padding: 10);

            return photo.GetBytes();
        }
    }
}