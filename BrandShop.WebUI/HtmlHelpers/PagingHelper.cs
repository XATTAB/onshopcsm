﻿using System;
using System.Text;
using System.Web.Mvc;
using BrandShop.WebUI.Models;

namespace BrandShop.WebUI.HtmlHelpers
{
    public static class PagingHelper
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingInfo pagingInfo, Func<int, string> pageUrl)
        {
            var result = new StringBuilder();
            for (var i = 1; i <= pagingInfo.TotalPages; i++)
            {
                var tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                var tagLi = new TagBuilder("li") {InnerHtml = tag.ToString()};

                if (i == pagingInfo.CurrentPage)                
                    tagLi.AddCssClass("active");
                              
                result.Append(tagLi);
            }
            return MvcHtmlString.Create(result.ToString());
        }
    }
}