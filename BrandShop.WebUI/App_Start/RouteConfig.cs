﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BrandShop.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Delivery", "Delivery", new {controller = "Information", action = "Delivery" });
            routes.MapRoute("Contacts", "Contacts", new { controller = "Information", action = "Contacts" });
            routes.MapRoute("News", "News", new { controller = "Information", action = "News" });
            routes.MapRoute("Index", "", new { controller = "Information", action = "Index" });

            routes.MapRoute("Admin/Orders", "Admin/Orders", new { controller = "AdminOrder", action = "Index" });
            routes.MapRoute("Admin", "Admin", new { controller = "AdminOrder", action = "Index" });
            routes.MapRoute("Admin/Catalog", "Admin/Catalog", new { controller = "Admin", action = "Index" });
            routes.MapRoute("Admin/News", "Admin/News", new { controller = "AdminNews", action = "Index" });
            routes.MapRoute("Admin/News/Edit", "Admin/News/Edit", new { controller = "AdminNews", action = "EditNews" });
            routes.MapRoute("Admin/Order/View", "Admin/Order/View", new { controller = "AdminOrder", action = "View" });
            routes.MapRoute("Admin/Catalog/Edit", "Admin/Catalog/Edit", new { controller = "Admin", action = "Edit" });

            routes.MapRoute(null, "Catalog",
                new
                {
                    controller = "Product",
                    action = "List",
                    category = (string) null,
                    page = 1
                }
                );

            routes.MapRoute(
                name: null,
                url: "Catalog/Page{page}",
                defaults: new {controller = "Product", action = "List", category = (string) null},
                constraints: new {page = @"\d+"}
                );

            routes.MapRoute(null,
                "Catalog/{category}",
                new {controller = "Product", action = "List", page = 1}
                );

            routes.MapRoute(null,
                "Catalog/{category}/Page{page}",
                new {controller = "Product", action = "List"},
                new {page = @"\d+"}
                );

            routes.MapRoute(null, "{controller}/{action}", namespaces: new string[] { "BrandShop.WebUI.Controllers" });
        }
    }
}
