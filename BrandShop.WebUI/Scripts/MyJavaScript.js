﻿function Update(actionUrl) {
    $('#results').load(actionUrl);
}

function Delete(newsIdp, urlRequest, actionUrl) {
    window.swal({
        title: 'Вы уверены?',
        text: "Удалив запись, вы не сможете её восстановить!",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Удалить!',
        cancelButtonText: 'Отменить'
    }).then(function () {
        window.swal(
            'Удалено!',
            '',
            'success'
        );
        $.get(urlRequest);
        setTimeout(Update(actionUrl), 1000);
    });
}

// переопределение валидации точки в числах на стороне клиента
$.validator.methods.range = function (value, element, param) {
    var globalizedValue = value.replace(",", ".");
    return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
}
$.validator.methods.number = function (value, element) {
    return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(value);
}