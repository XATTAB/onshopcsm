﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BrandShop.Domain.Entities;
using BrandShop.Domain.EntitiesDb;
using BrandShop.WebUI.Controllers;
using BrandShop.WebUI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BrandShop.Tests.Tests
{
    [TestClass]
    public class CartTests
    {
        private Cart _cart;

        [TestInitialize]
        public void Initialize()
        {
            var product1 = new Product { ProductId = 1, Name = "Игра 1", Price = 100 };
            var product2 = new Product { ProductId = 2, Name = "Игра 2", Price = 55};
            
            _cart = new Cart();
            
            _cart.AddItem(product1, 1, "");
            _cart.AddItem(product2, 3, "");
        }

        [TestMethod]
        public void Can_Add_New_Lines()
        {
            var results = _cart.Lines.ToList();
            
            Assert.AreEqual(results.Count, 2);
            Assert.AreEqual(results[0].Product.Name, "Игра 1");
            Assert.AreEqual(results[1].Product.Name, "Игра 2");
        }

        [TestMethod]
        public void Can_Add_Quantity_For_Existing_Lines()
        {
            var results = _cart.Lines.OrderBy(c => c.Product.ProductId).ToList();
            
            Assert.AreEqual(results.Count, 2);
            Assert.AreEqual(results[0].Quantity, 1);
            Assert.AreEqual(results[1].Quantity, 3);
        }

        [TestMethod]
        public void Can_Remove_Line()
        {
            var product2 = new Product { ProductId = 2, Name = "Игра 2" };
            
            _cart.RemoveLine(product2);
            
            Assert.AreEqual(_cart.Lines.Count(c => c.Product == product2), 0);
            Assert.AreEqual(_cart.Lines.Count(), 1);
        }
        
        [TestMethod]
        public void Calculate_Cart_Total()
        {
            var result = _cart.ComputeTotalValue();
            
            Assert.AreEqual(result, 265);
        }

        [TestMethod]
        public void Can_Clear_Contents()
        {
            _cart.Clear();
            
            Assert.AreEqual(_cart.Lines.Count(), 0);
        }
    }
}