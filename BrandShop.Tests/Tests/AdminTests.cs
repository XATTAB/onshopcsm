﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BrandShop.Domain.Entities;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;
using BrandShop.WebUI.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BrandShop.Tests.Tests
{
    [TestClass]
    public class AdminTests
    {
        private WebUI.Areas.Administration.Controllers.ProductController _adminController;

        [TestInitialize]
        public void Initialize()
        {
            var mock = new Mock<IUnitOfWork>();
            mock.Setup(m => m.ProductRepository.GetAll).Returns(new List<Product>
            {
                new Product {ProductId = 1, Name = "Игра1"},
                new Product {ProductId = 2, Name = "Игра2"},
                new Product {ProductId = 3, Name = "Игра3"},
                new Product {ProductId = 4, Name = "Игра4"},
                new Product {ProductId = 5, Name = "Игра5"}
            });
            
            _adminController = new WebUI.Areas.Administration.Controllers.ProductController(mock.Object);
        }

        [TestMethod]
        public void Index_Contains_All_Games()
        {
            var result = ((IEnumerable<Product>) _adminController.Index().ViewData.Model).ToList();

            // Утверждение
            Assert.AreEqual(result.Count, 5);
            Assert.AreEqual("Игра1", result[0].Name);
            Assert.AreEqual("Игра2", result[1].Name);
            Assert.AreEqual("Игра3", result[2].Name);
        }

        [TestMethod]
        public void Can_Edit_Game()
        {
            // Действие
            var game1 = _adminController.Edit(1).ViewData.Model as Product;
            var game2 = _adminController.Edit(2).ViewData.Model as Product;
            var game3 = _adminController.Edit(3).ViewData.Model as Product;

            // Assert
            Assert.AreEqual(1, game1.ProductId);
            Assert.AreEqual(2, game2.ProductId);
            Assert.AreEqual(3, game3.ProductId);
        }

        [TestMethod]
        public void Cannot_Edit_Nonexistent_Game()
        {
            var result = _adminController.Edit(6).ViewData.Model as Product;

            Assert.IsNull(result);
        }

        [TestMethod]
        public void Can_Save_Valid_Changes()
        {
            var mock = new Mock<IUnitOfWork>();
            var controller = new WebUI.Areas.Administration.Controllers.ProductController(mock.Object);
            var game = new Product {Name = "Test"};
            var result = controller.Edit(game);
            mock.Verify(m => m.ProductRepository.Create(game));
            Assert.IsNotInstanceOfType(result, typeof (ViewResult));
        }

        [TestMethod]
        public void Cannot_Save_Invalid_Changes()
        {
            // Организация - создание имитированного хранилища данных
            var mock = new Mock<IUnitOfWork>();

            // Организация - создание контроллера
            var controller = new WebUI.Areas.Administration.Controllers.ProductController(mock.Object);

            // Организация - создание объекта Game
            var game = new Product {Name = "Test"};

            // Организация - добавление ошибки в состояние модели
            controller.ModelState.AddModelError("error", "error");

            // Действие - попытка сохранения товара
            ActionResult result = controller.Edit(game);

            // Утверждение - проверка того, что обращение к хранилищу НЕ производится 
            mock.Verify(m => m.ProductRepository.Create(It.IsAny<Product>()), Times.Never());

            // Утверждение - проверка типа результата метода
            Assert.IsInstanceOfType(result, typeof (ViewResult));
        }

        [TestMethod]
        public void Can_Delete_Valid_Games()
        {
            // Организация - создание объекта Game
            var game = new Product {ProductId = 2, Name = "Игра2"};

            // Организация - создание имитированного хранилища данных
            var mock = new Mock<IUnitOfWork>();
            mock.Setup(m => m.ProductRepository.GetAll).Returns(new List<Product>
            {
                new Product {ProductId = 1, Name = "Игра1"},
                new Product {ProductId = 2, Name = "Игра2"},
                new Product {ProductId = 3, Name = "Игра3"},
                new Product {ProductId = 4, Name = "Игра4"},
                new Product {ProductId = 5, Name = "Игра5"}
            });

            // Организация - создание контроллера
            var controller = new WebUI.Areas.Administration.Controllers.ProductController(mock.Object);

            // Действие - удаление игры
            controller.Delete(game.ProductId);

            // Утверждение - проверка того, что метод удаления в хранилище
            // вызывается для корректного объекта Game
            mock.Verify(m => m.ProductRepository.Delete(game.ProductId));
        }
    }
}