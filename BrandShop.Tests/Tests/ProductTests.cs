﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BrandShop.Domain.Entities;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;
using BrandShop.WebUI.Controllers;
using BrandShop.WebUI.HtmlHelpers;
using BrandShop.WebUI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BrandShop.Tests.Tests
{
    [TestClass]
    public class ProductTests
    {
        private NavController _navController;
        private ProductController _productController;

        [TestInitialize]
        public void Initialize()
        {
            var mock = new Mock<IUnitOfWork>();
            mock.Setup(x => x.ProductRepository.GetAll).Returns(new List<Product>
            {
                new Product {ProductId = 1, Name = "Товар 1", Price = 10, Category = "Категория 1"},
                new Product {ProductId = 2, Name = "Товар 2", Price = 20, Category = "Категория 1"},
                new Product {ProductId = 3, Name = "Товар 3", Price = 30, Category = "Категория 2"},
                new Product {ProductId = 4, Name = "Товар 4", Price = 40, Category = "Категория 2"},
                new Product {ProductId = 5, Name = "Товар 5", Price = 50, Category = "Категория 3"},
                new Product {ProductId = 6, Name = "Товар 6", Price = 60, Category = "Категория 3"},
                new Product {ProductId = 7, Name = "Товар 7", Price = 70, Category = "Категория 3"}
            });

            _productController = new ProductController(mock.Object) { PageSize = 2 };
            _navController = new NavController(mock.Object);
        }

        [TestMethod]
        public void Can_Paginate()
        {
            var result = (ProductsListViewModel)_productController.List(null, OrderFilter.FirstPricey, 1).Model;

            List<Product> products = result.Products.ToList();
            Assert.IsTrue(products.Count == _productController.PageSize);
            Assert.AreEqual(products[0].Name, "Товар 7");
            Assert.AreEqual(products[1].Name, "Товар 6");
        }

        [TestMethod]
        public void Can_Generate_Page_Links()
        {
            // Организация - создание объекта PagingInfo
            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            // Организация - настройка делегата с помощью лямбда-выражения
            Func<int, string> pageUrlDelegate = i => "Page" + i;

            // Действие
            MvcHtmlString result = ((HtmlHelper) null).PageLinks(pagingInfo, pageUrlDelegate);

            // Утверждение
            Assert.AreEqual(
                "<li><a href=\"Page1\">1</a></li><li class=\"active\"><a href=\"Page2\">2</a></li><li><a href=\"Page3\">3</a></li>",
                result.ToString());
        }

        [TestMethod]
        public void Can_Send_Pagination_View_Model()
        {
            var result = (ProductsListViewModel)_productController.List(null, OrderFilter.FirstPricey, 1).Model;
            
            var pageInfo = result.PagingInfo;
            Assert.AreEqual(pageInfo.CurrentPage, 1);
            Assert.AreEqual(pageInfo.ItemsPerPage, 2);
            Assert.AreEqual(pageInfo.TotalItems, 7);
            Assert.AreEqual(pageInfo.TotalPages, 4);
        }

        [TestMethod]
        public void Can_Filter_Product()
        {
            var result = ((ProductsListViewModel)_productController.List("Категория 2").Model).Products.ToList();

            Assert.AreEqual(result.Count, 2);
            Assert.IsTrue(result[0].Name == "Товар 4" && result[0].Category == "Категория 2");
            Assert.IsTrue(result[1].Name == "Товар 3" && result[1].Category == "Категория 2");
        }

        [TestMethod]
        public void Can_Create_Categories()
        {
            var results = ((IEnumerable<string>)_navController.Menu().Model).ToList();
            
            Assert.AreEqual(results.Count, 3); // Количество категорий
            Assert.AreEqual(results[0], "Категория 1");
            Assert.AreEqual(results[1], "Категория 2");
            Assert.AreEqual(results[2], "Категория 3");
        }

        [TestMethod]
        public void Indicates_Selected_Category()
        {   
            var categoryToSelect = "Категория 1";

            var result = _navController.Menu(categoryToSelect).ViewBag.SelectedCategory;

            Assert.AreEqual(categoryToSelect, result);
        }

        [TestMethod]
        public void Generate_Category_Specific_Game_Count()
        {
            // Действие - тестирование счетчиков товаров для различных категорий
            var res1 = ((ProductsListViewModel) _productController.List("Категория 1").Model).PagingInfo.TotalItems;
            var res2 = ((ProductsListViewModel) _productController.List("Категория 2").Model).PagingInfo.TotalItems;
            var res3 = ((ProductsListViewModel) _productController.List("Категория 3").Model).PagingInfo.TotalItems;
            var resAll = ((ProductsListViewModel) _productController.List(null).Model).PagingInfo.TotalItems;

            Assert.AreEqual(res1, 2);
            Assert.AreEqual(res2, 2);
            Assert.AreEqual(res3, 3);
            Assert.AreEqual(resAll, 7);
        }
    }
}
