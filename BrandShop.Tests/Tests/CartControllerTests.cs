﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BrandShop.Domain.Entities;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;
using BrandShop.WebUI.Controllers;
using BrandShop.WebUI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BrandShop.Tests.Tests
{
    [TestClass]
    public class CartControllerTests
    {
        private Cart _cart;
        private CartController _controller;

       [TestInitialize]
        public void Initialize()
        {
            var mockOrderProcessor = new Mock<IOrderProcessor>();
            var mockUnit = new Mock<IUnitOfWork>();

            mockUnit.Setup(m => m.ProductRepository.GetAll).Returns(new List<Product>
            {
                new Product {ProductId = 1, Name = "Игра1", Category = "Кат1"},
            }.AsQueryable());
            
            _cart = new Cart();
            _controller = new CartController(mockOrderProcessor.Object, mockUnit.Object);
        }

        [TestMethod]
        public void Can_Add_To_Cart()
        {
            _controller.AddToCart(_cart, 1, null, null);
            
            Assert.AreEqual(_cart.Lines.Count(), 1);
            Assert.AreEqual(_cart.Lines.ToList()[0].Product.ProductId, 1);
        }

        /// <summary>
        /// После добавления игры в корзину, должно быть перенаправление на страницу корзины
        /// </summary>
        [TestMethod]
        public void Adding_Game_To_Cart_Goes_To_Cart_Screen()
        {
            // Действие - добавить игру в корзину
            var result = _controller.AddToCart(_cart, 2, "myUrl", null);

            // Утверждение
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.AreEqual(result.RouteValues["returnUrl"], "myUrl");
        }
        
        [TestMethod]
        public void Can_View_Cart_Contents()
        {
            var result = (CartIndexViewModel)_controller.Index(_cart, "myUrl").ViewData.Model;
            
            Assert.AreSame(result.Cart, _cart);
            Assert.AreEqual(result.ReturnUrl, "myUrl");
        }

        [TestMethod]
        public void Cannot_Checkout_Empty_Cart()
        {
            var shippingDetails = new ShippingDetails();
         
            var result = _controller.Checkout(_cart, shippingDetails);

            // Утверждение — проверка, что метод вернул стандартное представление 
            Assert.AreEqual("", result.ViewName);

            // Утверждение - проверка, что-представлению передана неверная модель
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Cannot_Checkout_Invalid_ShippingDetails()
        {
            var mock = new Mock<IOrderProcessor>();           
            _cart.AddItem(new Product(), 1, "");

            // Организация — добавление ошибки в модель
            _controller.ModelState.AddModelError("error", "error");

            // Действие - попытка перехода к оплате
            var result = _controller.Checkout(_cart, new ShippingDetails());

            // Утверждение - проверка, что метод вернул стандартное представление
            Assert.AreEqual("", result.ViewName);

            // Утверждение - проверка, что-представлению передана неверная модель
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Can_Checkout_And_Submit_Order()
        {
            _cart.AddItem(new Product(), 1, "XL");
            
            // Действие - попытка перехода к оплате
            var result = _controller.Checkout(_cart, new ShippingDetails
            {
                TypeDelivery = 0
            });

            // Утверждение - проверка, что метод возвращает представление 
            Assert.AreEqual("Completed", result.ViewName);

            // Утверждение - проверка, что представлению передается допустимая модель
            Assert.AreEqual(true, result.ViewData.ModelState.IsValid);
        }
    }
}