﻿using System;
using BrandShop.Domain.EntitiesDb;

namespace BrandShop.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<News> NewsRepository { get; }
        IRepository<Product> ProductRepository { get; }
        IRepository<Order> OrderRepository { get; }
        IRepository<ProductImage> ProductImageRepository { get; }
        IRepository<OrderItem> OrderItemRepository { get; }
        void Save();
    }
}