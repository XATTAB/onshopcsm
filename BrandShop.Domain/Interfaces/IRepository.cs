﻿using System;
using System.Collections.Generic;

namespace BrandShop.Domain.Interfaces
{
    public interface IRepository<T> where T: class
    {
        IEnumerable<T> GetAll { get; }
        T Get(int id);
        IEnumerable<T> GetWhere(Func<T, bool> predicate);
        void Create(T item);
        void Update(T item);
        T Delete(int id);
    }
}