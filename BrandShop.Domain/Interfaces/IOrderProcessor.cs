﻿using BrandShop.Domain.Entities;

namespace BrandShop.Domain.Interfaces
{
    public interface IOrderProcessor
    {
        void ProcessOrder(Cart cart, ShippingDetails shippingDetails);
    }
}