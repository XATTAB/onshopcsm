﻿using System.Data.Entity;
using BrandShop.Domain.EntitiesDb;

namespace BrandShop.Domain.Settings
{
    public class EfDbContext : DbContext
    {
        public EfDbContext()
        {
            Database.SetInitializer(new OnShopDbInitializer());
        }

        public DbSet<Product> Products { get; set; } 
        public DbSet<ProductImage> ProductImages { get; set; } 
        public DbSet<News> News { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
    }
}