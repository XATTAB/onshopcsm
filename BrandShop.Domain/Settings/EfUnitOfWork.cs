﻿using System;
using BrandShop.Domain.Concrete;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;

namespace BrandShop.Domain.Settings
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private readonly EfDbContext _dbContext;
        private IRepository<News> _newsRepository;
        private IRepository<Product> _productRepository;
        private IRepository<Order> _orderRepository;
        private IRepository<ProductImage> _productImageRepository;
        private IRepository<OrderItem> _orderLineRepository;

        public EfUnitOfWork()
        {
            _dbContext = new EfDbContext();
        }
        public IRepository<News> NewsRepository => _newsRepository ?? (_newsRepository = new EfNewsRepository(_dbContext));
        public IRepository<Product> ProductRepository => _productRepository ?? (_productRepository = new EfProductRepository(_dbContext));
        public IRepository<Order> OrderRepository => _orderRepository ?? (_orderRepository = new EfOrderRepository(_dbContext));
        public IRepository<ProductImage> ProductImageRepository => _productImageRepository ?? (_productImageRepository = new EfProductImageRepository(_dbContext));
        public IRepository<OrderItem> OrderItemRepository => _orderLineRepository ?? (_orderLineRepository = new EfOrderItemRepository(_dbContext));

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        #region Pattern Disposible
        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}