using System.Data.Entity.Migrations;

namespace BrandShop.Domain.Settings
{
    internal sealed class MigrationsConfiguration : DbMigrationsConfiguration<EfDbContext>
    {
        public MigrationsConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "BrandShop.Domain.Concrete.EfDbContext";
        }

        protected override void Seed(EfDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
