namespace BrandShop.Domain.Settings
{
    public class EmailSettings
    {
        public string MailToAddress { get; } = "st.classic@yandex.ru";
        public string MailFromAddress { get; } = "zakaz@street-classic.ru";
        public bool UseSsl { get; } = true;
        public string Username { get; } = "zakaz@street-classic.ru";
        public string Password { get; } = "qqq07081993";
        public string ServerName { get; } = "smtp.yandex.ru";
        public int ServerPort { get; } = 25;
        public bool WriteAsFile { get; set; } = false;
        public string FileLocation { get; } = @"d:\brand_shop_emails";
        public int Timeout { get; } = 5000;
    }
}