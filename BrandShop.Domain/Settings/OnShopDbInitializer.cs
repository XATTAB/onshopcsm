﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using BrandShop.Domain.EntitiesDb;

namespace BrandShop.Domain.Settings
{
    public class OnShopDbInitializer : DropCreateDatabaseIfModelChanges<EfDbContext>
    {
        protected override void Seed(EfDbContext context)
        {
            var news = new News {CreateDateTime = DateTime.Now, Header = "Добро пожаловать!", Text = "Тестовая запись!"};
            context.News.Add(news);

            var productOne = new Product
            {
                Category = "Игры",
                Description = "Описание",
                Name = "Call of Duty",
                Sizes = "122",
                Price = 1420
            };

            var productTwo = new Product
            {
                Category = "Игры",
                Description = "Описание",
                Name = "Call of Duty 2",
                Sizes = "132",
                Price = 2420
            };

            context.Products.Add(productOne);
            context.Products.Add(productTwo);

            var order = new Order
            {
                Address = "Адрес",
                Email = "Email",
                FullName = "Full Name",
                OrderStatus = OrderStatus.New,
                PhoneNumber = "8-954-78-78-741",
                OrderItems = new List<OrderItem>()
            };

            var orderItem = new OrderItem
            {
                Quantity = 2,
                Product = productOne,
                SizeProduct = "122"
            };

            order.OrderItems.Add(orderItem);

            context.Orders.Add(order);

            context.SaveChanges();
        }
    }
}