﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;
using BrandShop.Domain.Settings;

namespace BrandShop.Domain.Concrete
{
    public class EfOrderItemRepository : IRepository<OrderItem>
    {
        private readonly EfDbContext _context;

        public EfOrderItemRepository(EfDbContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<OrderItem> GetAll => _context.OrderItems;

        public OrderItem Get(int id) => _context.OrderItems.FirstOrDefault(x => x.OrderItemId == id);

        public IEnumerable<OrderItem> GetWhere(Func<OrderItem, bool> predicate) => _context.OrderItems.Where(predicate);

        public void Create(OrderItem item) => _context.OrderItems.Add(item);

        public void Update(OrderItem item) => _context.Entry(item).State = EntityState.Modified;

        public OrderItem Delete(int id)
        {
            var dbEntry = Get(id);

            if (dbEntry != null)
                _context.Entry(dbEntry).State = EntityState.Deleted;
            
            return dbEntry;
        }
    }
}