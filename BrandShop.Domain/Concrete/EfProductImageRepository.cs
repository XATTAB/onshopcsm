﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;
using BrandShop.Domain.Settings;

namespace BrandShop.Domain.Concrete
{
    public class EfProductImageRepository : IRepository<ProductImage>
    {
        private readonly EfDbContext _context;

        public EfProductImageRepository(EfDbContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<ProductImage> GetAll => _context.ProductImages;

        public ProductImage Get(int id) => _context.ProductImages.FirstOrDefault(x => x.ProductImageId == id);

        public IEnumerable<ProductImage> GetWhere(Func<ProductImage, bool> predicate)
            => _context.ProductImages.Where(predicate);

        public void Create(ProductImage item) => _context.ProductImages.Add(item);

        public void Update(ProductImage item) => _context.Entry(item).State = EntityState.Modified;

        public ProductImage Delete(int id)
        {
            var image = Get(id);

            if(image != null)
                _context.Entry(image).State = EntityState.Deleted;

            return image;
        }
    }
}