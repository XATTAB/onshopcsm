﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;
using BrandShop.Domain.Settings;

namespace BrandShop.Domain.Concrete
{
    public class EfProductRepository : IRepository<Product>
    {
        private readonly EfDbContext _context;

        public EfProductRepository(EfDbContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Product> GetAll => _context.Products.Include(x=>x.ProductImages);
   
        public Product Get(int id) => _context.Products.Include(x => x.ProductImages).FirstOrDefault(x => x.ProductId == id);

        public IEnumerable<Product> GetWhere(Func<Product, bool> predicate) => _context.Products.Include(x => x.ProductImages).Where(predicate);

        public void Create(Product item) => _context.Products.Add(item);

        public void Update(Product item) => _context.Entry(item).State = EntityState.Modified;

        public Product Delete(int productId)
        {
            var dbEntry = Get(productId);

            if (dbEntry != null)
            {
                var orderItems = _context.ProductImages.Where(x => x.ProductId == productId).ToList();

                foreach (var orderItem in orderItems)
                {
                    _context.Entry(orderItem).State = EntityState.Deleted;
                }
                _context.Entry(dbEntry).State = EntityState.Deleted;
            }

            return dbEntry;
        }
    }
}