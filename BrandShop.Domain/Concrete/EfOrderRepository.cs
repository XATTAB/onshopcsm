﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;
using BrandShop.Domain.Settings;

namespace BrandShop.Domain.Concrete
{
    public class EfOrderRepository : IRepository<Order>
    {
        private readonly EfDbContext _context;

        public EfOrderRepository(EfDbContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Order> GetAll => _context.Orders.Include(x=>x.OrderItems.Select(y=>y.Product));

        public Order Get(int id) => _context.Orders.Include(x => x.OrderItems.Select(y => y.Product)).FirstOrDefault(x => x.OrderId == id);

        public IEnumerable<Order> GetWhere(Func<Order, bool> predicate) => _context.Orders.Where(predicate);

        public void Create(Order item) => _context.Orders.Add(item);

        public void Update(Order item) => _context.Entry(item).State = EntityState.Modified;

        public Order Delete(int id)
        {
            var dbEntry = Get(id);
            if (dbEntry != null)
                _context.Orders.Remove(dbEntry);

            return dbEntry;
        }
    }
}