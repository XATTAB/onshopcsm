﻿using System.Net;
using System.Net.Mail;
using System.Text;
using BrandShop.Domain.Entities;
using BrandShop.Domain.Interfaces;
using BrandShop.Domain.Settings;

namespace BrandShop.Domain.Concrete
{
    public class EmailOrderProcessor : IOrderProcessor
    {
        private readonly EmailSettings _emailSettings;
        private ShippingDetails _shippingInfo;
        private Cart _cart;

        public EmailOrderProcessor(EmailSettings settings)
        {
            _emailSettings = settings;
        }

        private string GetOrderTextOfLetter()
        {
            var textLetter = new StringBuilder()
                    .AppendLine("Новый заказ на сайте street-classic.ru")
                    .AppendLine("---")
                    .AppendLine("Товары:");

            foreach (var line in _cart.Lines)
            {
                var subtotal = line.Product.Price * line.Quantity;
                textLetter.AppendFormat("{0} x {1} (итого: {2:c})", line.Quantity, line.Product.Name, subtotal);
                textLetter.AppendLine();
            }

            textLetter.AppendFormat("Общая стоимость: {0:c}", _cart.ComputeTotalValue())
                .AppendLine("")
                .AppendLine("---")
                .AppendLine("Доставка:")
                .AppendLine(_shippingInfo.FirstName)
                .AppendLine(_shippingInfo.Address)
                .AppendLine(_shippingInfo.PhoneNumber)
                .AppendLine(_shippingInfo.Email)
                .AppendLine("---");

            return textLetter.ToString();
        }

        private MailMessage GetMailMessage(string mailToAddress)
        {
            var mailMessage = new MailMessage(
                _emailSettings.MailFromAddress, mailToAddress, 
                "Новый заказ оформлен!", GetOrderTextOfLetter());

            if (_emailSettings.WriteAsFile)            
                mailMessage.BodyEncoding = Encoding.UTF8; 

            return mailMessage;
        }

        public void ProcessOrder(Cart cart, ShippingDetails shippingInfo)
        {
            _shippingInfo = shippingInfo;
            _cart = cart;

            using (var smtpClient = new SmtpClient())
            {
                smtpClient.EnableSsl = _emailSettings.UseSsl;
                smtpClient.Host = _emailSettings.ServerName;
                smtpClient.Port = _emailSettings.ServerPort;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Timeout = _emailSettings.Timeout;
                smtpClient.Credentials = new NetworkCredential(_emailSettings.Username, _emailSettings.Password);

                if (_emailSettings.WriteAsFile)
                {
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    smtpClient.PickupDirectoryLocation = _emailSettings.FileLocation;
                    smtpClient.EnableSsl = false;
                }
                
                smtpClient.Send(GetMailMessage(shippingInfo.Email));
                smtpClient.Send(GetMailMessage(_emailSettings.MailToAddress));
            }
        }
    }
}