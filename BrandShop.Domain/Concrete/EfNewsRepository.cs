﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BrandShop.Domain.EntitiesDb;
using BrandShop.Domain.Interfaces;
using BrandShop.Domain.Settings;

namespace BrandShop.Domain.Concrete
{
    public class EfNewsRepository : IRepository<News>
    {
        private readonly EfDbContext _context;

        public EfNewsRepository(EfDbContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<News> GetAll => _context.News;

        public News Get(int id) => _context.News.FirstOrDefault(x => x.NewsId == id);

        public IEnumerable<News> GetWhere(Func<News, bool> predicate) => _context.News.Where(predicate);

        public void Create(News item) => _context.News.Add(item);

        public void Update(News item) => _context.Entry(item).State = EntityState.Modified;

        public News Delete(int newsId)
        {
            var dbEntry = _context.News.Find(newsId);
            if (dbEntry != null)            
                _context.News.Remove(dbEntry);
            
            return dbEntry;
        }
    }
}