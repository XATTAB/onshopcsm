﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BrandShop.Domain.EntitiesDb
{
    public class News
    {
        public int NewsId { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Пожалуйста, введите название новости")]
        public string Header { get; set; }

        [Display(Name = "Содержимое")]
        [Required(ErrorMessage = "Пожалуйста, введите содержимое новости")]
        public string Text { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreateDateTime { get; set; } 
    }
}