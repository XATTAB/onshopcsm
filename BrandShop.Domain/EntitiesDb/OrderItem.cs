﻿namespace BrandShop.Domain.EntitiesDb
{
    public class OrderItem
    {
        public int OrderItemId { get; set; }
        
        public Product Product { get; set; }

        public int Quantity { get; set; }

        public string SizeProduct { get; set; }
    }
}