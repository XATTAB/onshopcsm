﻿namespace BrandShop.Domain.EntitiesDb
{
    public class ProductImage
    {
        public int ProductImageId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; } 
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
        public byte[] PreviewImageData { get; set; }
        public string PreviewImageMimeType { get; set; }
    }
}