﻿using System.ComponentModel;

namespace BrandShop.Domain.EntitiesDb
{
    public enum OrderStatus
    {
        [Description("Новый")]
        New = 0,

        [Description("В обработке")]
        Processing = 1,

        [Description("Доставлено")]
        Delivered = 2,

        [Description("Не дозвонились")]
        NotTake = 3
    }
}