﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;


namespace BrandShop.Domain.EntitiesDb
{
    public class Product
    {
        public Product()
        {
            ProductImages = new List<ProductImage>();
        }

        [HiddenInput(DisplayValue = false)]
        public int ProductId { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Пожалуйста, введите название товара")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Описание")]
        [Required(ErrorMessage = "Пожалуйста, введите описание для товара")]
        public string Description { get; set; }

        [Display(Name = "Категория")]
        [Required(ErrorMessage = "Пожалуйста, укажите категорию для товара")]
        public string Category { get; set; }

        [Display(Name = "Цена (руб)")]
        [Required]
        [Range(0.01, double.MaxValue, ErrorMessage = "Пожалуйста, введите положительное значение для цены")]
        public decimal Price { get; set; }

        public List<ProductImage> ProductImages { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreateDateTime { get; set; }

        [Display(Name = "Хит")]
        public bool IsHit { get; set; }

        [Display(Name = "Распродажа")]
        public bool IsSale { get; set; }

        [Display(Name = "Старая цена")]
        [Range(0.00, double.MaxValue, ErrorMessage = "Пожалуйста, введите положительное значение для цены")]
        public decimal OldPrice { get; set; }

        [Display(Name = "Для главной")]
        public bool ForMain { get; set; }

        [Display(Name = "Размер")]
        public string Sizes { get; set; }
    }
}