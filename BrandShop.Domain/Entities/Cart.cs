﻿using System.Collections.Generic;
using System.Linq;
using BrandShop.Domain.EntitiesDb;

namespace BrandShop.Domain.Entities
{
    public class Cart
    {
        private readonly List<OrderItem> _orderLines = new List<OrderItem>();

        public IEnumerable<OrderItem> Lines => _orderLines.ToList();

        public void AddItem(Product product, int quantity, string size)
        {
            var line = _orderLines.FirstOrDefault(g => g.Product.ProductId == product.ProductId && g.SizeProduct == size);

            if (line == null)
            {
                _orderLines.Add(new OrderItem
                {
                    Product = product,
                    Quantity = quantity,
                    SizeProduct = size
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public void RemoveLine(Product game)
        {
            _orderLines.RemoveAll(l => l.Product.ProductId == game.ProductId);
        }

        public decimal ComputeTotalValue()
        {
            return _orderLines.Sum(e => e.Product.Price * e.Quantity);
        }

        public void Clear()
        {
            _orderLines.Clear();
        }
    }
}