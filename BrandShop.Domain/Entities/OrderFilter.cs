﻿namespace BrandShop.Domain.Entities
{
    public enum OrderFilter
    {
        FirstNew = 0,
        FirstCheap = 1,
        FirstPricey = 2
    }
}