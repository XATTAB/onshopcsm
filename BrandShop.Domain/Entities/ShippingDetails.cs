﻿using System.ComponentModel.DataAnnotations;

namespace BrandShop.Domain.Entities
{
    public class ShippingDetails
    {
        [Required(ErrorMessage = "Укажите как вас зовут")]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Укажите вашу фамилию")]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Укажите адрес доставки")]
        [Display(Name = "Адрес доставки")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Укажите номер телефона")]
        [Display(Name = "Номер телефона")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Укажите E-mail")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        public int? TypeDelivery { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }
}